<html>
    <head>
        <title>CIS 322 REST-api</title>
    </head>

    <body>
        <h1>Times in DB</h1>

        <h3>API Response</h3>
        <?php
            // execute when form is submitted
            if ( isset($_GET['submit']) ) {
                // get provided variables and define uri
                $selection = $_GET['selection'];
                $type = $_GET['type'];
                $limit = $_GET['limit'];
                $uri = "/";

                // add times selection to uri
                if ($selection == "open") {
                    $uri .= "listOpenOnly";
                } elseif ($selection == "close") {
                    $uri .= "listCloseOnly";
                } else {
                    $uri .= "listAll";
                }

                // add data type to uri
                if ($type == "csv") {
                    $uri .= "/csv";
                } else {
                    $uri .= "/json";
                }

                // add limit on data
                if ($limit != "") {
                    $uri .= "?top=" . $limit;
                }

                // get api resource at particular uri
                $uri = 'http://localhost:5000' . $uri;
                $data = file_get_contents($uri);
                echo $data;
            }
        ?>

        <br/>
        <br/>
        <br/>
        <br/>

        <!-- Form for easily grabbing and viewing api info -->
        <h3>API Data Selection</h3>
        <form>
            <table>
                <tr>
                    Time Selection:
                    <select name="selection">
                        <option value="all">All Times</option>
                        <option value="open">Open Times Only</option>
                        <option value="close">Close Times Only</option>
                    </select>
                </tr>
                <br/>
                <tr>
                    Data Type:
                    <select name="type">
                        <option value="json">json</option>
                        <option value="csv">csv</option>
                    </select>
                </tr>
                <br/>
                <tr>
                    Number Results:
                    <input type=number name="limit" placeholder="ex: 9"/>
                </tr>
                <br/>
                <tr>
                    <input type=submit name="submit" value="Submit" />
                </tr>
            </table>
        </form>

    </body>
</html>
