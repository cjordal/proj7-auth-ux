"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, request, render_template, redirect, flash
from flask_restful import Resource, Api, abort   # api interaction
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient # mongodb connection
import flask_login as login
from flask_login import login_required
from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired
from passlib.apps import custom_app_context as pwd_context          # password hashing
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer      # token generation/checking
import itsdangerous as t_check      # token errors
import logging


###########
# Globals
###########

# define token properties
SECRET_TOKEN_KEY = "testing@123?!"
TOKEN_LIFESPAN = 300

app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

# add restful functionality to flask app
api = Api(app)

# add flask login to flask app
login_manager = login.LoginManager()
login_manager.init_app(app)

# configure csrf protection for flask app
csrf = CSRFProtect(app)

# connect to database
#client = MongoClient("mongodb://localhost:27017")
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017) # <<<<<<<<<<<<<<<<< REMEMBER TO SWITCH
time_db = client.times
user_db = client.users


##############
# USER LOGIC
##############

# your user class
class User(login.UserMixin):
    def __init__(self, name, u_id, active=True, hash_vals=None, token=None):
        self.id = u_id
        self.name = name
        self.hash = hash_vals       # store hashed password with user
        self.token = token          # save a token with user
        self.active = active

    def is_active(self):
        return self.active

# note that the ID returned must be unicode
USERS = {
    1: User(u"guest", 1)
}
_users = user_db.users.find()
for u_info in _users:
    # import users w/ user database info
    USERS[u_info["id"]] = User(u_info["username"], u_info["id"], hash_vals=u_info["hash"])
USER_NAMES = dict((u.name, u) for u in USERS.values())


@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))


####################
# USEFUL FUNCTIONS
####################

def add_user(username, password):
    """Add users to user dicts and database
    NOTE: doesn't work if USERS has no users"""
    # find id for new user
    next_user_id = max(USERS.keys()) + 1
    # hash users password
    hash_vals = pwd_context.encrypt(password)
    # store new user info in database
    user_info = {'id': next_user_id, 'username': username, 'hash': hash_vals}
    user_db.users.insert_one(user_info)
    # update user dicts
    next_user = User(username, next_user_id, hash_vals=hash_vals)
    USERS[next_user_id] = next_user
    USER_NAMES[username] = next_user
    return next_user_id


def generate_auth_token(user_id, expiration=600):
    # generate general token
    s = Serializer(SECRET_TOKEN_KEY, expires_in=expiration)
    # pass index of user
    return s.dumps({'id': user_id})

def verify_auth_token(token):
    # check if token present
    if token is None:
        return False
    # check if token is valid
    s = Serializer(SECRET_TOKEN_KEY)
    try:
        data = s.loads(token)
    except t_check.SignatureExpired:
        return False    # valid token, but expired
    except t_check.BadSignature:
        return False    # invalid token
    return True


def make_csv_line(data: list) -> str:
    line = ""
    # add each item with a ',' at the end
    for item in data:
        line += str(item) + ','
    # replace trailing ',' with newline char
    if len(line) > 0:
        line = line[:-1] + '\n'
    return line


############
# ROUTING
############

class UserInfo(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


@app.route('/', methods=('GET', 'POST'))
def user_auth():
    """Register/Sign-in new users"""
    form = UserInfo()
    if form.validate_on_submit():
        username = form.username.data
        print(f"User: {username}")
        if username in USER_NAMES:
            flash("User already exists!")
            return render_template('submit.html', form=form)
        add_user(username, form.password.data)
    return render_template('submit.html', form=form)


@app.route('/sign_in', methods=('GET', 'POST'))
def sign_in():
    """Sign in as an existing user"""
    form = UserInfo()
    if form.validate_on_submit():
        username = form.username.data
        print(f"User: {username} signing in")
        print(USER_NAMES.keys())
        if username not in USER_NAMES:
            flash(f"Invalid username")
            return redirect('/')
        print(f"User: {username} is valid")
        user = USER_NAMES[username]
        user_hash = user.hash
        given_pwd = form.password.data
        # login users with no passwords and users with valid passwords
        if user_hash is None:
            login.login_user(user, remember=True)
            return redirect('/main')
        elif pwd_context.verify(given_pwd, user_hash):
            login.login_user(user, remember=True)
            # generate new token for user each sign in
            user.token = generate_auth_token(user.id, TOKEN_LIFESPAN)
            return redirect('/main')
        else:
            flash("Invalid password")
    return redirect('/')


@app.route("/main")
@login_required
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.route('/logout')
@login_required
def logout():
    login.logout_user()
    flash("Successfully logged out")
    return redirect('/')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


#########################
# AJAX request handlers
#########################

def make_arrow(date: str, time: str) -> "Arrow":
    year = int(date[:4])
    month = int(date[5:7])
    day = int(date[8:])
    hour = int(time[:2])
    minute = int(time[3:])
    return arrow.Arrow(year, month, day, hour, minute)

@app.route("/_calc_times")
@login_required
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    app.logger.debug("request.args: {}".format(request.args))
    # get input brevet distance for race
    distance = request.args.get("brevet", type=str)
    # remove "km" off string ("200km" -> 200) and turn into int
    distance = int(distance[:-2])
    app.logger.debug("distance={}".format(distance))
    # get the input km for the race
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    race_date = request.args.get("date", type=str)
    app.logger.debug("race start date={}".format(race_date))
    race_time = request.args.get("time", type=str)
    app.logger.debug("race start time={}".format(race_time))
    start_race = make_arrow(race_date, race_time)
    app.logger.debug("iso-format race start={}".format(start_race.isoformat()))
    open_time = acp_times.open_time(km, distance, start_race)
    close_time = acp_times.close_time(km, distance, start_race)
    result = {"open": open_time, "close": close_time}
    app.logger.debug(f"times: {result}")
    return flask.jsonify(result=result)


######################
# MongoDB Interaction
######################

@app.route("/_store")
@login_required
def _save_times():
    raw_times = request.args.to_dict()
    app.logger.debug(f"times={raw_times}")
    # calculate num times to insert
    num_times = len(raw_times)/2
    # don't insert empty time data
    if num_times > 0:
        times = {"open": [], "close": []}
        for time in raw_times:
            if raw_times[time] == "open":
                times["open"].append(time)
            else:
                times["close"].append(time)
        # remove old times
        time_db.times.remove({})
        # add new time
        time_db.times.insert_one(times)
    # send number of open/close times inserted
    return flask.jsonify(result={"ok": num_times})


@app.route("/times")
@login_required
def display_times():
    _items = time_db.times.find()
    item = next(_items)
    num_times = len(item["open"])
    return flask.render_template("times.html", times=item, num_times=num_times)



######################
# API FUNCTIONALITY
######################

@app.route('/api/register', methods=('GET', 'POST'))
def user_reg():
    """Register/Sign-in new users"""
    user_info = dict(request.args)
    username = user_info['username']
    print(f"User: {username}")
    # stop if user already exists
    if username in USER_NAMES:
        abort(400, msg="user already exists")
    user_id = add_user(username, user_info['password'])
    return flask.jsonify({"id", user_id}), 201


@app.route('/api/token', methods=('GET', 'POST'))
def get_token():
    """Sign in as an existing user"""
    user_info = dict(request.args)
    username = user_info["username"]
    print(f"User: {username} signing in")
    # abort if not a user
    if username not in USER_NAMES:
        abort(401)
    user = USER_NAMES[username]
    user_hash = user.hash
    given_pwd = user_info["password"]
    if pwd_context.verify(given_pwd, user_hash):
        # generate new token for user
        user.token = generate_auth_token(user.id, TOKEN_LIFESPAN)
        return flask.jsonify({"token": user.token, "duration": TOKEN_LIFESPAN})
    # abort if user doesn't sign in appropriately
    abort(401)


class Times(Resource):
    method_decorators = [login_required]

    def __init__(self):
        self.valid_groups = ["listAll", "listOpenOnly", "listCloseOnly"]
        self.valid_types = ["csv", "json"]

    def __get_data(self, group: str) -> dict:
        """Gets entries from mongodb and return desired data"""
        # get times from database
        # select only open times
        if group == "listOpenOnly":
            _items = time_db.times.find({}, {"open": 1})
        # select only close times
        elif group == "listCloseOnly":
            _items = time_db.times.find({}, {"close": 1})
        # select all times
        else:
            _items = time_db.times.find()
        times = next(_items)
        # remove id class from dict
        times.pop("_id")
        return times

    def __limit_data(self, times: dict, limit: int) -> dict:
        """Limit number of times in dict to a selected number"""
        # don't alter data when no limit
        if limit is not None:
            # get time category (e.g. open)
            labels = list(times.keys())
            # shrink times to limit
            for label in labels:
                # only shrink when limit is smaller than length
                num_ts = len(times[label])
                if limit < num_ts:
                    times[label] = times[label][:limit]
        return times

    def __convert_csv(self, times: dict) -> str:
        """Create a csv string from a dictionary"""
        # get desired keys
        labels = list(times.keys())
        # create labels on first line
        csv_data = make_csv_line(labels)
        # add each line of time data
        num_times = len(times[labels[0]])
        for ti in range(num_times):
            line_data = []
            for label in labels:
                line_data.append(times[label][ti])
            time_line = make_csv_line(line_data)
            csv_data += time_line
        return csv_data

    def get(self, group="listAll", dtype="json"):
        # only allow access by users with valid tokens
        user_token = login.current_user.token
        if not verify_auth_token(user_token):
            abort(401)
        # check if valid info is requested
        if group not in self.valid_groups:
            abort(404, msg="invalid dataset requested")
        if dtype not in self.valid_types:
            abort(404, msg="invalid return type requested")
        # create default limit
        limit = None
        # change limit if one was given
        uri_data = dict(request.args)
        if "top" in uri_data:
            try:
                limit = int(uri_data["top"])
            except ValueError:
                abort(404, msg="limit must be of type int")
        # get desired data
        times = self.__get_data(group)
        times = self.__limit_data(times, limit)
        # return data in desired format
        if dtype == "csv":
            return self.__convert_csv(times)
        else:
            return flask.jsonify(times)


# Create routes
# Another way, without decorators
api.add_resource(Times, '/<group>', '/<group>/<dtype>')  # <<<<<<<<<<<<<< FINISH THIS!!!!




#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
